package com.wiredbrain.friends.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data

@Embeddable
public class Address {

    private int locId;

    public int getLoc_id() {
        return locId;
    }

    public void setLoc_id(int loc_id) {
        this.locId = locId;
    }

    private String street;
    private String city;

}

