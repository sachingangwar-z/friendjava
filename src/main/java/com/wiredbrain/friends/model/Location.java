package com.wiredbrain.friends.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GeneratorType;
import org.springframework.boot.autoconfigure.web.WebProperties;

import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data

public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int locId;
    private String location;

    private int friendId;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "friendId",referencedColumnName = "friendId")
    List<Friend> friends=new ArrayList<>();



}