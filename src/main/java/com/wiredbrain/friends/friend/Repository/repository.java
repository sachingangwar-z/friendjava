package com.wiredbrain.friends.friend.Repository;

import com.wiredbrain.friends.model.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface repository extends JpaRepository<Friend,Integer> {
    Iterable<Friend> findByFirstNameAndLastName(String firstName, String lastName);

    Iterable<Friend> findByFirstName(String firstName);

    Iterable<Friend> findByLastName(String lastName);
}
