package com.wiredbrain.friends.friend.service;

import com.wiredbrain.friends.friend.DTO.LocationConvertor;
import com.wiredbrain.friends.friend.DTO.LocationRequestDto;
import com.wiredbrain.friends.friend.DTO.LocationResponseDto;
import com.wiredbrain.friends.friend.DTO.ResponseDto;
import com.wiredbrain.friends.friend.Repository.LocationRepository;
import com.wiredbrain.friends.model.Friend;
import com.wiredbrain.friends.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LocationService {
    @Autowired
    private LocationRepository locationRepo;

    @Autowired
    private LocationConvertor locationConvertor;

    public List<LocationResponseDto> getAllLocation() {

        List<Location> locations = locationRepo.findAll();
        List<LocationResponseDto> ResponseDTOList = new ArrayList<>();
        for (Location location : locations)
            ResponseDTOList.add(locationConvertor.ModelToResponse(location));
        return ResponseDTOList;
    }

    public ResponseEntity<String> addNewLocation(LocationRequestDto requestDto) {
        try {
            locationRepo.save(locationConvertor.RequestToModel(requestDto));
            return new ResponseEntity<>("location Added Successfully...", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("location Not Added, Please Try Again...", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity<String> updateExistingLocation(int id, LocationRequestDto requestDto) {
        Location location = locationConvertor.RequestToModel(requestDto);
        Optional<Location> LocationIsPresent = locationRepo.findById(id);

        if (LocationIsPresent.isPresent()) {
            Location locations = LocationIsPresent.get();
            locations.setLocId(location.getLocId());
            locations.setLocation(location.getLocation());
            locationRepo.save(locations);

            return new ResponseEntity<>("location Updated Successfully...", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Id not Found, Please Try Again..", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity<String> deleteById(Integer lid) {
        try {
            Optional<Location> location = locationRepo.findById(lid);
            locationRepo.delete(location.get());
            return new ResponseEntity<>("location Deleted Successfully...", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Id not Found, Please Try Again..", HttpStatus.NOT_FOUND);
        }
    }


    public LocationResponseDto findById(int id)
    {
        Optional<Location> location=locationRepo.findById(id);
        if(location.isPresent())
        {
            return locationConvertor.ModelToResponse(location.get());
        }
        return null;
    }
}
