package com.wiredbrain.friends.friend.service;

import com.wiredbrain.friends.friend.DTO.Convertor;
import com.wiredbrain.friends.friend.DTO.RequestDto;
import com.wiredbrain.friends.friend.DTO.ResponseDto;
import com.wiredbrain.friends.friend.Repository.repository;
import com.wiredbrain.friends.model.Friend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FriendService {
    @Autowired
    private repository repo;

    @Autowired
    private Convertor conversion;

    public List<ResponseDto> getAllFriend() {

        List<Friend> friends = repo.findAll();
        List<ResponseDto> ResponseDTOList = new ArrayList<ResponseDto>();
        for (Friend friend : friends)
            ResponseDTOList.add(conversion.ModelToResponse(friend));
        return ResponseDTOList;
    }

    public ResponseEntity<String> addNewFriend(RequestDto RequestDTO) {
        try {
            repo.save(conversion.RequestToModel(RequestDTO));
            return new ResponseEntity<>("User Added Successfully...", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("User Not Added, Please Try Again...", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<String> deleteById(int Id) {

        try {
            Optional<Friend> friend = repo.findById(Id);
            repo.delete(friend.get());
            return new ResponseEntity<>("User Deleted Successfully...", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Id not Found, Please Try Again..", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseDto findById(int Id) {
        Friend friend = repo.findById(Id).orElseThrow(() -> new IllegalStateException("Task with ID " + Id + " is not present"));
        ResponseDto ResponseDTO = conversion.ModelToResponse(friend);
        return ResponseDTO;
    }

    public ResponseEntity<String> updateExistingFriend(int id, RequestDto requestDto) {
        Friend friend = conversion.RequestToModel(requestDto);
        Optional<Friend> friendIsPresent = repo.findById(id);

        if (friendIsPresent.isPresent()) {
            Friend friends = friendIsPresent.get();
            friends.setLastName(friend.getLastName());
            friends.setFirstName(friend.getFirstName());
            friends.setAddress(friend.getAddress());
            friends.setMarried(friend.isMarried());
            friends.setAge(friend.getAge());
            repo.save(friends);

            return new ResponseEntity<>("User Updated Successfully...", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Id not Found, Please Try Again..", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}




