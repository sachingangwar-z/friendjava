package com.wiredbrain.friends.friend.controller;

import com.wiredbrain.friends.friend.DTO.RequestDto;
import com.wiredbrain.friends.friend.DTO.ResponseDto;
import com.wiredbrain.friends.friend.service.FriendService;
import com.wiredbrain.friends.model.Friend;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;



@RestController
public class FriendController {
    @Autowired
    private final FriendService friendService;

    public FriendController(FriendService friendService) {
        this.friendService = friendService;
    }

    @PostMapping("/friends")
    ResponseEntity<String> createNewFriend(@RequestBody RequestDto requestDto){

        return friendService.addNewFriend(requestDto);

    }
// @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    List<FieldErrorMessage> exceptionHandler(final MethodArgumentNotValidException e){
//        List<FieldError>fieldErrors= (List<FieldError>) e.getBindingResult().getFieldError();
//        List<FieldErrorMessage>fieldErrorMessages= (List<FieldErrorMessage>) fieldErrors.stream().map(fieldError -> new FieldErrorMessage(fieldError.getField(),fieldError.getDefaultMessage())).collect(Collectors.toList());
//
//        return fieldErrorMessages;
//    }

    @GetMapping("/friends")
    Iterable<ResponseDto> GetAllFriend(){
        return friendService.getAllFriend();
    }

    @PutMapping("/friends/{id}")
    ResponseEntity<String> UpdateExistingFriend(@PathVariable Integer id , @RequestBody RequestDto requestDto){
        return friendService.updateExistingFriend(id,requestDto);

    }

    @DeleteMapping("/friends/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Integer id){

        return friendService.deleteById(id);
    }
    @GetMapping("/friends/{id}")
    Optional<ResponseDto> findById(@PathVariable Integer id){

        return Optional.ofNullable(friendService.findById(id));
    }


    }

