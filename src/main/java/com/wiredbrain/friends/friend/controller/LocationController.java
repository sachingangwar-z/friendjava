package com.wiredbrain.friends.friend.controller;

import com.wiredbrain.friends.friend.DTO.LocationRequestDto;
import com.wiredbrain.friends.friend.DTO.LocationResponseDto;
import com.wiredbrain.friends.friend.DTO.ResponseDto;
import com.wiredbrain.friends.friend.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class LocationController {
    @Autowired
    private LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/locations")
    Iterable<LocationResponseDto> GetAllFriend(){
        return locationService.getAllLocation();
    }

    @PostMapping("/locations")
    ResponseEntity<String> createNewLocation(@RequestBody LocationRequestDto requestDto){

        return locationService.addNewLocation(requestDto);

    }

    @PutMapping("/locations/{id}")
    ResponseEntity<String> UpdateExistingLocation(@PathVariable Integer lid , @RequestBody LocationRequestDto requestDto){
        return locationService.updateExistingLocation(lid,requestDto);

    }
    @DeleteMapping("/locations/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Integer id){

        return locationService.deleteById(id);
    }

    @GetMapping("/locations/{id}")
    public ResponseEntity<LocationResponseDto> getUser(@PathVariable int id)
    {
        LocationResponseDto ResponseDto =locationService.findById(id);
        if(ResponseDto !=null) return new ResponseEntity(ResponseDto, HttpStatus.OK);
        else return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
    }
}
