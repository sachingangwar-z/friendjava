package com.wiredbrain.friends.friend.DTO;

import com.wiredbrain.friends.model.Address;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RequestDto {
    private String firstName;
    private String lastName;
    private int age;
    private boolean married;
    private Address address;


    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }
}
