package com.wiredbrain.friends.friend.DTO;

import com.wiredbrain.friends.model.Friend;
import org.springframework.stereotype.Component;

@Component
public class Convertor {
    public Friend RequestToModel(RequestDto RequestDTO){
        Friend model = new Friend();
        model.setFirstName(RequestDTO.getFirstName());
        model.setLastName(RequestDTO.getLastName());
        model.setAddress(RequestDTO.getAddress());
        model.setAge(RequestDTO.getAge());
        model.setMarried(RequestDTO.isMarried());
        return model;
    }

    public ResponseDto ModelToResponse(Friend model){
        ResponseDto ResponseDTO = new ResponseDto();
        ResponseDTO.setFirstName(model.getFirstName());
        ResponseDTO.setLastname(model.getLastName());
        ResponseDTO.setAddress(model.getAddress());



        return ResponseDTO;
    }


}
