package com.wiredbrain.friends.friend.DTO;

import com.wiredbrain.friends.model.Address;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ResponseDto {
    private String firstName;
    private String lastname;
    private Address address;
}
