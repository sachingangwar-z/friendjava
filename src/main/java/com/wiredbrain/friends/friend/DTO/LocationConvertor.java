package com.wiredbrain.friends.friend.DTO;

import com.wiredbrain.friends.model.Friend;
import com.wiredbrain.friends.model.Location;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LocationConvertor {
    public Location RequestToModel(LocationRequestDto LocationRequestDTO){
        Location model = new Location();
        model.setLocId(LocationRequestDTO.getLoc_id());
        model.setLocation(LocationRequestDTO.getLocation());
        return model;
    }

    public LocationResponseDto ModelToResponse(Location model){
        LocationResponseDto ResponseDTO = new LocationResponseDto();
        ResponseDTO.setLocation(model.getLocation());
        List<Friend> friends=new ArrayList<>();
        for(Friend friend: model.getFriends())
        {
            friends.add(friend);
        }
        ResponseDTO.setFriends(friends);
        return ResponseDTO;
    }
}
