package com.wiredbrain.friends.friend.DTO;

import com.wiredbrain.friends.model.Friend;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LocationResponseDto {
    private String location;
    private List<Friend> friends=new ArrayList<>();
}
